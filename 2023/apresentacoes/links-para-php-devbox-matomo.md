# Links para as palestras do Joubert Guimarães de Assis "RedRat"

## Retome o controle do seu analytics com Matomo:

<https://speakerdeck.com/joubertredrat/retome-o-controle-do-seu-analytics-com-matomo>

## #tbt do PHP, a evolução da linguagem nos últimos 30 anos:

<https://speakerdeck.com/joubertredrat/number-tbt-do-php-a-evolucao-da-linguagem-nos-ultimos-30-anos>

## DevBox, o meu pequeno projeto open source para o dia a dia do dev:
<https://speakerdeck.com/joubertredrat/devbox-o-meu-pequeno-projeto-open-source-para-o-dia-a-dia-do-dev>


